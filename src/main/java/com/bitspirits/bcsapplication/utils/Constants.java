package com.bitspirits.bcsapplication.utils;

import java.util.ArrayList;

public class Constants {

    public static final String KEY_MESSAGE = "message";
    public static final String KEY_STATUS = "status";
    public static final String KEY_DATA = "data";
    public static final String KEY_TOKEN = "token";

    public static final String TOKEN_SECRET_KEY = "bcs2";
    public static final long TOKEN_VALIDATION = 1 * 365 * 24 * 60 * 60 * 1000;

    public static final String KEY_USER_ID = "id";
    public static final String KEY_USER_CATEGORY_ID = "category_id";
    public static final String KEY_USER_EMAIL = "email";

    public static final String MODEL_TEST_SERVER = "localhost:8085/api/v1";
    public static final String AUTH_SERVER = "localhost:8080/api/v1";
    public static final String QUESTION_SERVER = "localhost:8081/api/v1";

    private static final ArrayList<String> whitelisted_urls = new ArrayList<>();

    public static ArrayList<String> getWhitelisted_urls() {
        whitelisted_urls.clear();
        whitelisted_urls.add("/api/v1/category/**");
        return whitelisted_urls;
    }
}
