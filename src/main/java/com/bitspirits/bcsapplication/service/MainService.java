package com.bitspirits.bcsapplication.service;

import com.bitspirits.bcsapplication.model.*;
import com.bitspirits.bcsapplication.utils.Constants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import javax.servlet.ServletRequest;
import java.util.*;

@Service
public class MainService implements IMainService {

    @Autowired
    private WebClient.Builder builder;

    @Autowired
    private ServletRequest request;

    @Override
    public Object registration(Map<String, String> map) {
        Object  o = builder.build().post()
                .uri(Constants.AUTH_SERVER+"/users/register")
                .body(Mono.just(map), Object.class)
                .retrieve()
                .bodyToMono(Object.class)
                .block();
        return o;
    }


    @Override
    public Object login(Map<String, String> map) {
        Object  o = builder.build().post()
                .uri(Constants.AUTH_SERVER+"/users/login")
                .body(Mono.just(map), Object.class)
                .retrieve()
                .bodyToMono(Object.class)
                .block();
        return o;
    }

    @Override
    public Object categories() {
        Object  o = builder.build().get()
                .uri(Constants.QUESTION_SERVER+"/category/all")
                .retrieve()
                .bodyToMono(Object.class)
                .block();
        return o;
    }

    @Override
    public Object subjects() {
        Object  o = builder.build().get()
                .uri(Constants.QUESTION_SERVER+"/subject/all")
                .header("Authorization", "Bearer "+request.getAttribute("token"))
                .retrieve()
                .bodyToMono(Object.class)
                .block();
        return o;
    }

    @Override
    public Object getModelTestById(String id) {
        ApiResponseModel  model_test_response = builder.build().get()
                .uri(Constants.MODEL_TEST_SERVER+"/model_test/"+id)
                .header("Authorization", "Bearer "+request.getAttribute("token"))
                .retrieve()
                .bodyToMono(ApiResponseModel.class)
                .block();

        for (Map.Entry<String,Object> entry : ((LinkedHashMap<String,Object>) model_test_response.getData()).entrySet()){

            if (entry.getKey().equals("subjects")){

                ApiResponseModel subjects_with_questions_details = builder.build().post()
                        .uri(Constants.QUESTION_SERVER+"/question/question_sheet")
                        .body(Mono.just(entry.getValue()),Object.class)
                        .header("Authorization", "Bearer "+request.getAttribute("token"))
                        .retrieve()
                        .bodyToMono(ApiResponseModel.class)
                        .block();

                entry.setValue(subjects_with_questions_details);
            }

        }

        return model_test_response;
    }

    @Override
    public Object createModelTest(Map<String, String> map) {

        String subjects = map.get("subject");
        String type = map.get("type");
        int time = Integer.parseInt(map.get("time"));


        Map<String, String> map1 = new HashMap<>();
        map1.put("subject",subjects);
        map1.put("limit",String.valueOf(3*time));


        ApiResponseModel subjects_with_questions = builder.build().post()
                .uri(Constants.QUESTION_SERVER+"/question/filter")
                .body(Mono.just(map1),Object.class)
                .header("Authorization", "Bearer "+request.getAttribute("token"))
                .header("Content-Type", "application/json")
                .retrieve()
                .bodyToMono(ApiResponseModel.class)
                .block();


        ArrayList<Participants> participants = new ArrayList<>();
        participants.add(new Participants("1"));

        ModelTest modelTest = new ModelTest();
        modelTest.setTitle("Single model test");
        modelTest.setStartTime(new Date());
        modelTest.setEndTime(new Date(new Date().getTime()+(time*60*1000)));
        modelTest.setTime(time);
        modelTest.setType(new Type("1"));

        ModelTestDto modelTestDto = new ModelTestDto();
        modelTestDto.setModelTest(modelTest);
        modelTestDto.setSubjects((ArrayList<Object>) subjects_with_questions.getData());
        modelTestDto.setParticipants(participants);


        ApiResponseModel model_test_response = builder.build().post()
                .uri(Constants.MODEL_TEST_SERVER+"/model_test/save")
                .body(Mono.just(modelTestDto),ModelTestDto.class)
                .header("Authorization", "Bearer "+request.getAttribute("token"))
                .header("Content-Type", "application/json")
                .retrieve()
                .bodyToMono(ApiResponseModel.class)
                .block();


        for (Map.Entry<String,Object> entry : ((LinkedHashMap<String,Object>) model_test_response.getData()).entrySet()){

            if (entry.getKey().equals("subjects")){

                ApiResponseModel subjects_with_questions_details = builder.build().post()
                        .uri(Constants.QUESTION_SERVER+"/question/question_sheet")
                        .body(Mono.just(entry.getValue()),Object.class)
                        .header("Authorization", "Bearer "+request.getAttribute("token"))
                        .retrieve()
                        .bodyToMono(ApiResponseModel.class)
                        .block();

                entry.setValue(subjects_with_questions_details);
            }

        }



//        Object o = builder.build().get()
//                .uri(uriBuilder ->
//                        uriBuilder.path("localhost:8081/api/v1/question/filter")
//                                .queryParam("subject",subjects)
//                                .queryParam("limit","3")
//                                .build()
//                )
//                .header("Authorization", "Bearer "+request.getAttribute("token"))
//                .header("Content-Type", "application/json")
//                .retrieve()
//                .bodyToMono(Object.class)
//                .block();



        return model_test_response;
    }

}
