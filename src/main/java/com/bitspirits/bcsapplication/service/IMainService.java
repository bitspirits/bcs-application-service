package com.bitspirits.bcsapplication.service;

import com.bitspirits.bcsapplication.model.SubjectsWithQuestions;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

public interface IMainService {
    Object registration(Map<String ,String > map);
    Object login(Map<String ,String > map);
    Object categories();
    Object subjects();
    Object createModelTest(Map<String ,String > map);
    Object getModelTestById(String id);
}
