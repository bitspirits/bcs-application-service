package com.bitspirits.bcsapplication;

import com.bitspirits.bcsapplication.security.AuthFilter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication
public class BcsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BcsApplication.class, args);
	}

	@Bean
	public WebClient.Builder getWebClientBuilder(){
		return WebClient.builder();
	}

	@Bean
	public FilterRegistrationBean<AuthFilter> FilterRegistrationBean(){
		FilterRegistrationBean<AuthFilter> registrationBean =  new FilterRegistrationBean<>();
		registrationBean.setFilter(new AuthFilter());
		return registrationBean;
	}


}
