package com.bitspirits.bcsapplication.controller;

import com.bitspirits.bcsapplication.model.SubjectsWithQuestions;
import com.bitspirits.bcsapplication.service.IMainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/v1/")
public class MainController {

    @Autowired
    private IMainService service;

    @PostMapping("registration")
    public Object registration(@RequestBody Map<String ,String > map){
        return service.registration(map);
    }

    @PostMapping("login")
    public Object login(@RequestBody Map<String ,String > map){
        return service.login(map);
    }

    @GetMapping("categories")
    public Object getAllCategories(){
        return service.categories();
    }

    @GetMapping("subjects")
    public Object getAllSubjects(){
        return service.subjects();
    }

    @GetMapping("model_test/{id}")
    public Object getModelTestById(@PathVariable("id") String id){
        return service.getModelTestById(id);
    }

    @PostMapping("modelTest")
    public Object createModelTest(@RequestBody Map<String ,String> map){
        return service.createModelTest(map);
    }


}
