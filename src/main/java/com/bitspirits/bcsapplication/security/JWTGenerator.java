package com.bitspirits.bcsapplication.security;

import com.bitspirits.bcsapplication.utils.Constants;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

public class JWTGenerator {

    public static Claims getClaimsByToken(String token){
        return  Jwts.parser().setSigningKey(Constants.TOKEN_SECRET_KEY).parseClaimsJws(token).getBody();
    }
}
