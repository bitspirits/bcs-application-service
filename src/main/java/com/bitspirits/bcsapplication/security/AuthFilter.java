package com.bitspirits.bcsapplication.security;

import com.bitspirits.bcsapplication.utils.Constants;
import io.jsonwebtoken.Claims;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        HttpServletResponse response = httpServletResponse;
        HttpServletRequest request = httpServletRequest;

        String header = request.getHeader("Authorization");

        if (header != null) {
            String[] headerArr = header.split("Bearer");


            try {
                String token = headerArr[1];
                request.setAttribute("token", token);

              //  Claims claims = JWTGenerator.getClaimsByToken(token);

             //   request.setAttribute(Constants.KEY_USER_ID, claims.get(Constants.KEY_USER_ID));
            //    request.setAttribute(Constants.KEY_USER_EMAIL, claims.get(Constants.KEY_USER_EMAIL));

            } catch (Exception e) {
                response.sendError(HttpStatus.FORBIDDEN.value(), "Please provide valid token");
                return;
            }
        }

        filterChain.doFilter(request, response);
    }
}
