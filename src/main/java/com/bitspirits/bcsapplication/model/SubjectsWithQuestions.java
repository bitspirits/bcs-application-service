package com.bitspirits.bcsapplication.model;

import javax.swing.*;
import java.util.ArrayList;

public class SubjectsWithQuestions {
    private String id;
    private ArrayList<Question> questions;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }
}
