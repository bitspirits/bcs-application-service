package com.bitspirits.bcsapplication.model;

public class Participants {
    private String id;

    public Participants(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
