package com.bitspirits.bcsapplication.model;

import java.util.ArrayList;

public class ModelTestDto {
    private ModelTest modelTest;
    private ArrayList<Object> subjects;
    private ArrayList<Participants> participants;

    public ModelTest getModelTest() {
        return modelTest;
    }

    public void setModelTest(ModelTest modelTest) {
        this.modelTest = modelTest;
    }

    public ArrayList<Object> getSubjects() {
        return subjects;
    }

    public void setSubjects(ArrayList<Object> subjects) {
        this.subjects = subjects;
    }

    public ArrayList<Participants> getParticipants() {
        return participants;
    }

    public void setParticipants(ArrayList<Participants> participants) {
        this.participants = participants;
    }
}
