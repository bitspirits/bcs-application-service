package com.bitspirits.bcsapplication.model;

import java.util.List;

public class DtoSubjectWithQuestion {
    private long id;
    private String name;
    private List<DtoQuestion> questions;

    public DtoSubjectWithQuestion(long id, String name, List<DtoQuestion> questions) {
        this.id = id;
        this.name = name;
        this.questions = questions;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DtoQuestion> getQuestions() {
        return questions;
    }

    public void setQuestions(List<DtoQuestion> questions) {
        this.questions = questions;
    }
}
