package com.bitspirits.bcsapplication.model;

import com.bitspirits.bcsapplication.utils.Constants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

public class ResponseModel {
    private Map<String, Object> map= new HashMap<>();
    private HttpStatus httpStatus;

    public ResponseModel(HttpStatus httpStatus, String message, Boolean status) {
        this.httpStatus = httpStatus;
        map.put(Constants.KEY_MESSAGE, message);
        map.put(Constants.KEY_STATUS, status);
    }




    public ResponseModel setData(Object object){
        map.put(Constants.KEY_DATA, object);
        return this;
    }

    public ResponseModel setToken(Object object){
        map.put(Constants.KEY_TOKEN, object);
        return this;
    }

    public ResponseModel setAttribute(String key, String value){
        map.put(key, value);
        return this;
    }

    public ResponseEntity<Map<String, Object>> build(){
        return new ResponseEntity<>(map, httpStatus);
    }
}
