package com.bitspirits.bcsapplication.model;

import java.util.List;

public class DtoQuestion {
    private long id;
    private String title;
    private long sectionId;
    private String sectionName;
    private long topicId;
    private String topicName;
    private List<DtoOption> options;

    public DtoQuestion(long id, String title, long sectionId, String sectionName, long topicId, String topicName, List<DtoOption> options) {
        this.id = id;
        this.title = title;
        this.sectionId = sectionId;
        this.sectionName = sectionName;
        this.topicId = topicId;
        this.topicName = topicName;
        this.options = options;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getSectionId() {
        return sectionId;
    }

    public void setSectionId(long sectionId) {
        this.sectionId = sectionId;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public long getTopicId() {
        return topicId;
    }

    public void setTopicId(long topicId) {
        this.topicId = topicId;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public List<DtoOption> getOptions() {
        return options;
    }

    public void setOptions(List<DtoOption> options) {
        this.options = options;
    }

    /*Subject subject;

    public DtoQuestion(Subject subject, int categoryId) {
        this.subject = subject;

        this.subject.setQuestions(this.subject.getQuestions().stream()
                .filter(q -> q.getCategory().getId() == categoryId)
                .collect(Collectors.toList()));

        this.subject.setCategories(new ArrayList<>());
//        this.subject.setQuestions((List<Question>) question);
        this.subject.getQuestions().forEach(question ->
        {
            question.getCategory().setSubjects(new ArrayList<>());
            question.setSubject(null);
            question.getSection().setTopics(new ArrayList<>());
            question.getSection().setSubjects(new ArrayList<>());
            question.getTopic().setSections(new ArrayList<>());
            question.getOptions().forEach(option -> option.setQuestion(null));

        });

    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }
*/
}
