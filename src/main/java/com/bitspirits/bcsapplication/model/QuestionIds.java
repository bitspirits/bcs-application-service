package com.bitspirits.bcsapplication.model;

public class QuestionIds {
    private String questionId;

    public QuestionIds(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }
}
