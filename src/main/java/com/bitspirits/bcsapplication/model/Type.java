package com.bitspirits.bcsapplication.model;

public class Type {
    private String id;

    public Type(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
