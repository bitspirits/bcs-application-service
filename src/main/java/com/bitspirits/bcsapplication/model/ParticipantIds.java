package com.bitspirits.bcsapplication.model;

public class ParticipantIds {
    private String userId;

    public ParticipantIds(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
